import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;


public class GetWinnerTests {

    void assertWinner(int expectedWinner, int[][]board){
        int actual = Player.getWinner(board);

        assertEquals(expectedWinner, actual);
    }


    @Test
    void emptyBoard() {
        assertWinner(0, new int[][]{
                {0,0,0},
                {0,0,0},
                {0,0,0}
        });
    }

    @Test
    void topRowWinner() {
        assertWinner(1, new int[][]{
                {1, 1, 1},
                {1, 0, 2},
                {0, 1, 2}});
    }

    @Test
    void middleColumnWinner(){
        int[][] board = {
                {0,1,0},
                {0,1,0},
                {2,1,0}
        };

        int expected = 1;
        int actual = Player.getWinner(board);

        assertEquals(expected, actual);
    }

    @Test
    void middleRowWinnerpPlayer2(){

            assertWinner(  2, new int[][]{
                    {1,0,1},
                    {2,2,2},
                    {0,0,1}});
        }


    @Test
    void middleRowWinner(){
        assertWinner( 1, new int[][]{
                {1,0,1},
                {1,1,1},
                {0,0,2}});
    }

    @Test
    void bottomRowWinner(){
        assertWinner(  1, new int[][]{
                {1,0,1},
                {0,2,2},
                {1,1,1}});
    }

    @Test
    void leftDiagonalWinner(){

        assertWinner( 1, new int[][]{
                {1,0,1},
                {2,1,0},
                {2,0,1}});
    }

    @Test
    void rightDiagonalWinner(){

        assertWinner(1, new int[][]{
                {0,0,1},
                {2,1,0},
                {1,0,1}});
    }


}
